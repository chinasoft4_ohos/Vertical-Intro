﻿1.0.0
--------------
正式版本

0.0.3-SNAPSHOT
--------------
新增对外方法

| 方法名                   | 作用                     |
|-------------------------|--------------------------------------------------------|
| addIntroItem(VerticalIntroItem item) | 添加构建的项目           |
| backgroundColor(int color) | 设置页面背景色           |
| image(int resourceId) | 设置顶部图标，引用资源ID                              |
| title(String title) | 设置标题文字                                     |
| titleColor(int color) | 设置标题颜色                   |
| titleSize(float size) | 设置标题大小                             |
| text(String text) | 设置内容文字                             |
| textColor(int color) | 设置内容颜色             |
|textSize(float size)|设置内容大小|
|build()|设置构建参数|
|setSkipEnabled(boolen isSkipEnabled)|设置是否显示跳过按钮|
|setVibrateEnabled(boolen isVibrateEnabled)|设置是否点击有振动反馈|
|setSkipColor(int skipColor)|设置跳过按钮文字颜色|
|setNextText(String text)|设置下一步文字|
|setDoneText(String text)|设置完成文字|
|setSkipText(String text)|设置跳过文字|
|setVibrateIntensity(int intensity)|设置点击振动强度|
|setCustomTypeFace(String fontPath)|设置字体路径|


0.0.2-SNAPSHOT
--------------
升级sdk

0.0.1-SNAPSHOT
--------------
ohos 第一个版本

完整实现了原库的全部 api，支持的功能：
* 设置标题、内容的文字、大小、颜色
* 设置点击时振动反馈
* 设置页面背景、图标