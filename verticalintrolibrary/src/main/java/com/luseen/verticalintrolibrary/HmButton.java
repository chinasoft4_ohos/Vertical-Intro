/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a  copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.luseen.verticalintrolibrary;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 自定义button
 *
 * @since 2021-05-10
 */
public class HmButton extends Button implements Component.TouchEventListener {
    private static final String TAG = "HmButton";
    private String clickColor;

    private String normalColor;
    private int trance;

    /**
     *
     * 构造函数
     *
     * @param context context对象
     */
    public HmButton(Context context) {
        super(context);
    }

    /**
     *
     * 构造函数
     *
     * @param context context对象
     * @param attrSet 属性值
     */
    public HmButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        mContext = context;
        clickColor = attrSet.getAttr("clickColor").get().getStringValue();
        normalColor = attrSet.getAttr("normalColor").get().getStringValue();
        trance = attrSet.getAttr("trance").get().getIntegerValue();
        this.setTouchEventListener(this::onTouchEvent);
    }

    /**
     *
     * 构造函数
     *
     * @param context context对象
     * @param attrSet 属性值
     * @param styleName 属性名
     */
    public HmButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 改变背景色
     *
     * @param normalColor 颜色值
     */
    public void setNormalColor(String normalColor) {
        this.normalColor = normalColor;
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(normalColor)));
        this.setBackground(shapeElement);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                ShapeElement element = new ShapeElement();
                element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(clickColor)));
                this.setBackground(element);
                return true;
            case TouchEvent.POINT_MOVE:
                return false;
            case TouchEvent.PRIMARY_POINT_UP:
                ShapeElement backelement = new ShapeElement();
                backelement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(normalColor)));
                backelement.setAlpha(trance);
                this.setBackground(backelement);
                return true;
            default:
        }
        return true;
    }
}
