package com.luseen.verticalintrolibrary;

/**
 * Created by Chatikyan on 19.10.2016.
 */

public class VerticalIntroItem{
    private String title;
    private String text;
    private int backgroundColor;
    private int nextTextColor;
    private int titleColor;
    private int textColor;
    private int image;
    private float textSize;
    private float titleSize;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public int getNextTextColor() {
        return nextTextColor;
    }

    public void setNextTextColor(int nextTextColor) {
        this.nextTextColor = nextTextColor;
    }

    public int getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(int titleColor) {
        this.titleColor = titleColor;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public float getTitleSize() {
        return titleSize;
    }

    public void setTitleSize(float titleSize) {
        this.titleSize = titleSize;
    }

    private VerticalIntroItem(Builder builder) {
        this.title = builder.title;
        this.text = builder.text;
        this.image = builder.image;
        this.backgroundColor = builder.backgroundColor;
        this.textSize = builder.textSize;
        this.textColor = builder.textColor;
        this.titleSize = builder.titleSize;
        this.titleColor = builder.titleColor;
        this.nextTextColor = builder.nextTextColor;
    }

    public static class Builder {
        private String title;
        private String text;
        private int nextTextColor = 0XFFFFFFFF;
        private int titleColor = 0XFFFFFFFF;
        private int textColor = 0XFFFFFFFF;
        private int backgroundColor;
        private int image;
        private float titleSize;
        private float textSize;

        public Builder nextTextColor(int nextTextColor) {
            this.nextTextColor = nextTextColor;
            return this;
        }

        public Builder textColor(int textColor) {
            this.textColor = textColor;
            return this;
        }

        public Builder titleColor(int titleColor) {
            this.titleColor = titleColor;
            return this;
        }

        public Builder textSize(float textSize) {
            this.textSize = textSize;
            return this;
        }

        public Builder titleSize(float titleSize) {
            this.titleSize = titleSize;
            return this;
        }

        public Builder() {
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder text(String text) {
            this.text = text;
            return this;
        }

        public Builder backgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public Builder image(int image) {
            this.image = image;
            return this;
        }

        public VerticalIntroItem build() {
            return new VerticalIntroItem(this);
        }
    }
}
