package com.luseen.verticalintrolibrary;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class VerticalIntro extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(VerticalIntroSlice.class.getName());
    }
}
