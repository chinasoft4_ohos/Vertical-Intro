/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a  copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.luseen.verticalintrolibrary;

import ohos.agp.colors.RgbColor;

/**
 * 自定义button
 *
 * @since 2021-06-30
 */
public class PageMoudle {
    private int recoureId;
    private int backGroundColor;
    private String title;
    private String text;
    private int titleSize;
    private int textSize;
    private RgbColor backGroudRgbColor;

    public int getRecoureId() {
        return recoureId;
    }

    public void setRecoureId(int recoureId) {
        this.recoureId = recoureId;
    }

    public int getBackGroundColor() {
        return backGroundColor;
    }

    public void setBackGroundColor(int backGroundColor) {
        this.backGroundColor = backGroundColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTitleSize() {
        return titleSize;
    }

    public void setTitleSize(int titleSize) {
        this.titleSize = titleSize;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public RgbColor getBackGroudRgbColor() {
        return backGroudRgbColor;
    }

    public void setBackGroudRgbColor(RgbColor backGroudRgbColor) {
        this.backGroudRgbColor = backGroudRgbColor;
    }
}
