package com.luseen.verticalintrolibrary;

import com.luseen.verticalintro.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class VerticalIntroPagerAdapter extends PageSliderProvider {
    private List<VerticalIntroItem> datas;
    private Context context;
    private Component component;
    public List<Component> pageComonent;
    LinkedHashMap<Integer, Component> pageComonents;
    Font font;
    public VerticalIntroPagerAdapter(Context context, List<VerticalIntroItem> datas ,Font customFont) {
        this.context = context;
        this.datas = datas;
        pageComonent = new ArrayList<>();
        pageComonents = new LinkedHashMap<Integer, Component>();
        font = customFont;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_pageslider_item, null, true);
        Image image = (Image) component.findComponentById(ResourceTable.Id_sliderImage);
        Text title = (Text) component.findComponentById(ResourceTable.Id_title);
        Text text = (Text) component.findComponentById(ResourceTable.Id_text);
        if (i == datas.size()-1){
            image.setPixelMap(datas.get(3).getImage());
        }else {
            VectorElement element = new VectorElement(context, datas.get(i).getImage());
            image.setImageElement(element);
        }
        title.setTextColor(new Color(datas.get(i).getTitleColor()));
        if(datas.get(i).getTitleSize()!=0){
            title.setTextSize(AttrHelper.fp2px(datas.get(i).getTitleSize(),context));
        }
        title.setText(datas.get(i).getTitle());

        text.setTextColor(new Color(datas.get(i).getTextColor()));
        if(datas.get(i).getTextSize()!=0){
            text.setTextSize(AttrHelper.fp2px(datas.get(i).getTextSize(),context));
        }
        text.setText(datas.get(i).getText());

        if(font!=null){
            title.setFont(font);
            text.setFont(font);
        }

        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(datas.get(i).getBackgroundColor()));
        component.setBackground(element);
        component.setTag(i);
        if (!pageComonents.containsValue(component)) {
            pageComonents.put(i, component);
        }
        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return true;
    }
}
