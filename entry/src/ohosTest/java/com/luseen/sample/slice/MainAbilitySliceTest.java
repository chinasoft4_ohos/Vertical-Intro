/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a  copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.luseen.sample.slice;

import junit.framework.TestCase;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MainAbilitySliceTest extends TestCase {
    private Class unitClass;
    private Object obj;

    /**
     * 初始化被测试类
     */
    private void initClass() {
        try {
            unitClass = Class.forName("com.luseen.sample.slice.MainAbilitySlice");
            obj = unitClass.getConstructor().newInstance();   //实例化一个对象
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
            | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建顶部的折叠view
     */
    @Test
    public void testSetData() {
        initClass();
        try {
            Method method = unitClass.getMethod("setData", Object.class);
            method.invoke(obj, new Object[]{});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetData() {
        initClass();
        try {
            Method method = unitClass.getMethod("getData", Object.class);
            method.invoke(obj, new Object[]{});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}