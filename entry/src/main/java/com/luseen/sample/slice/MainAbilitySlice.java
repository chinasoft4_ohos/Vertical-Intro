/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a  copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.luseen.sample.slice;

import com.luseen.verticalintro.ResourceTable;
import com.luseen.verticalintrolibrary.VerticalIntroItem;
import com.luseen.verticalintrolibrary.VerticalIntroSlice;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;

import java.util.ArrayList;

public class MainAbilitySlice extends VerticalIntroSlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void setData() {
        super.setData();
        pageColors = new ArrayList<>();
        pageColors.add(getString(ResourceTable.Color_colorAccent));
        pageColors.add(getString(ResourceTable.Color_color2));
        pageColors.add(getString(ResourceTable.Color_colorPrimary));
        pageColors.add(getString(ResourceTable.Color_color3));

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(0)))
                .image(ResourceTable.Graphic_intro_second_vector)
                .title("Lorem Ipsum Lorem Ipsum")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .textSize(14)
                .titleSize(18)
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(1)))
                .image(ResourceTable.Graphic_four)
                .title("Lorem Ipsum Lorem Ipsum ")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(2)))
                .image(ResourceTable.Graphic_ohos)
                .title("Lorem Ipsum")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .textColor(Color.BLACK.getValue())
                .titleColor(Color.BLACK.getValue())
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(3)))
                .image(ResourceTable.Media_new_intro)
                .title("Lorem Ipsum")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .build());

        setSkipEnabled(true);
        setVibrateEnabled(true);
        setSkipColor(Color.BLACK.getValue());
//        setNextText("OK");
//        setDoneText("FINISH HIM");
//        setSkipText("GO GO");
        setVibrateIntensity(20);
        setCustomTypeFace("resources/rawfile/NotoSans-Regular.ttf");
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
