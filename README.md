﻿# Vertical-Intro

#### 项目介绍
- 项目名称：Vertical-Intro
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现通用的应用引导功能
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本: Release 2.0

#### 效果演示

![](gif/screen.gif) 

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation 'com.gitee.chinasoft_ohos:ohos-verticalintro:1.0.0'
    ......  
 }
 ```

在sdk6，DevEco Studio 2.2 Bate1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1、自己的AbilitySlice继承VerticalIntroSlice，在setData方法中添加数据

```java

public class MainAbilitySlice extends VerticalIntroSlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void setData() {
        super.setData();
        pageColors = new ArrayList<>();
        pageColors.add(getString(ResourceTable.Color_colorAccent));
        pageColors.add(getString(ResourceTable.Color_color2));
        pageColors.add(getString(ResourceTable.Color_colorPrimary));
        pageColors.add(getString(ResourceTable.Color_color3));

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(0)))
                .image(ResourceTable.Graphic_intro_second_vector)
                .title("Lorem Ipsum Lorem Ipsum")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .textSize(14)
                .titleSize(18)
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(1)))
                .image(ResourceTable.Graphic_four)
                .title("Lorem Ipsum Lorem Ipsum ")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(2)))
                .image(ResourceTable.Graphic_ohos)
                .title("Lorem Ipsum")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .textColor(Color.BLACK.getValue())
                .titleColor(Color.BLACK.getValue())
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(Color.getIntColor(pageColors.get(3)))
                .image(ResourceTable.Media_new_intro)
                .title("Lorem Ipsum")
                .text("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                .build());

        setSkipEnabled(true);
        setVibrateEnabled(true);
        setSkipColor(Color.BLACK.getValue());
//        setNextText("OK");
//        setDoneText("FINISH HIM");
//        setSkipText("GO GO");
        setVibrateIntensity(20);
        setCustomTypeFace("resources/rawfile/NotoSans-Regular.ttf");
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
```

2、方法作用说明

| 方法名                   | 作用                     |
|-------------------------|--------------------------------------------------------|
| addIntroItem(VerticalIntroItem item) | 添加构建的项目           |
| backgroundColor(int color) | 设置页面背景色           |
| image(int resourceId) | 设置顶部图标，引用资源ID                              |
| title(String title) | 设置标题文字                                     |
| titleColor(int color) | 设置标题颜色                   |
| titleSize(float size) | 设置标题大小                             |
| text(String text) | 设置内容文字                             |
| textColor(int color) | 设置内容颜色             |
|textSize(float size)|设置内容大小|
|build()|设置构建参数|
|setSkipEnabled(boolen isSkipEnabled)|设置是否显示跳过按钮|
|setVibrateEnabled(boolen isVibrateEnabled)|设置是否点击有振动反馈|
|setSkipColor(int skipColor)|设置跳过按钮文字颜色|
|setNextText(String text)|设置下一步文字|
|setDoneText(String text)|设置完成文字|
|setSkipText(String text)|设置跳过文字|
|setVibrateIntensity(int intensity)|设置点击振动强度|
|setCustomTypeFace(String fontPath)|设置字体路径|

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

     Vertical Intro library for Android
     Copyright (c) 2017 Arman Chatikyan (https://github.com/armcha/Vertical-Intro).
     
     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at
     
      http://www.apache.org/licenses/LICENSE-2.0
     
     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.